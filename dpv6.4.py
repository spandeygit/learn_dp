valid_words = {"apple", "pie", "eat", "delicious", "is", "a", "food", "dish"}
sentence = "applepieisdelicious"

def is_valid_sentence(sentence):
    n = len(sentence)
    V = [0] * (n + 1)
    V[0] = 1  # Base case

    for i in range(1, n + 1):
        for k in range(1, i + 1):
            # Check if the substring S[k:i] is 
            # a valid word and the transition is allowed
            # NOTE technically this is k:i but python indexes string in 0 based index
            if ((sentence[k-1:i] in valid_words) and V[k-1] == 1):
                V[i] = 1
                break

    return V[n] == 1

# Example sentence

# Check if the sentence can be formed by valid words
result = is_valid_sentence(sentence)
print(result)

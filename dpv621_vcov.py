from collections import defaultdict
 
class Graph:
 
    def __init__(self):
        self.graph = defaultdict(list)
        self.vcov = defaultdict(list) ; # (inc,exc) for each node
 
    def addEdge(self, u, v):
        self.graph[u].append(v)
 
    def VCUtil(self, v, visited):
        visited.add(v)
        #print(v, end=' ')
        for neighbour in self.graph[v]:
            if neighbour not in visited:
                self.VCUtil(neighbour, visited)
        # Now run the vertex cover algorithm
        if (len(self.graph[v]) == 0) :
            #print("Leaf node found %d\n"%(v))
            self.vcov[v]  = [1,0]
            return(0);
            
        else :
            #print("Parent node found %d\n"%(v))
            inc = 1;
            exc = 0;
            for neighbour in self.graph[v]:
                # now collect all options of children nodes 
                # include 
                #print("Solving min for child %d"%(neighbour));
                inc +=  min(self.vcov[neighbour]);
                # exclude 
                exc += self.vcov[neighbour][0]; # include 
            self.vcov[v]  = [inc,exc]
            #print("Vertex cover size at %d is %d\n"%(v,min([inc, exc])));
            return(min([inc,exc]));




 
    def DFS(self, v):
        visited = set()
        t = self.VCUtil(v, visited)
        print("Vertex cover size at %d is %d"%(v,t));
 
g = Graph()
# 0 - B
# 1 - A 
# 2 - C 
# 3 - E
# 4 - D
# 5 - F 
# 6 - G
g.addEdge(0, 1)
g.addEdge(0, 2)
g.addEdge(1, 3)
g.addEdge(3, 4)
g.addEdge(3, 5)
g.addEdge(5, 6)
 
g.DFS(0)
 

# BASE DFS from 
# https://www.geeksforgeeks.org/depth-first-search-or-dfs-for-a-graph/
